#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
using namespace eosio;

class teleos : public eosio::contract {
  public:
      teleos(account_name s):
        contract(s), // initialization of the base class for the contract
        _people(s, s) // initialize the table with code and scope NB! Look up definition of code and scope
      {
      }

      /// @abi action
      void create(account_name from, account_name to, const std::string& data) {
        require_auth(from);
        print("123");
        // Let's make sure the primary key doesn't exist
        // _people.end() is in a way similar to null and it means that the value isn't found
        // eosio_assert(_people.find(id) == _people.end(), "This id already exists in the teleos");
        _people.emplace(get_self(), [&]( auto& p ) {
           p.id = _people.available_primary_key();
           p.data = data;
           p.sender = from;
           p.receiver = to;
        });
      }

  private:
    // Setup the struct that represents the row in the table
    /// @abi table messages i64
    struct person {
      uint64_t id; // primary key
      std::string data;


      account_name sender;
      uint32_t ddm;
      account_name receiver;

      uint64_t primary_key()const { return id; }
      uint64_t by_sender()const { return sender; }
    };

    // We setup the table:

    typedef eosio::multi_index< N(messages), person, indexed_by<N(bysender), const_mem_fun<person, uint64_t, &person::by_sender>>>  people;

    people _people;

};

 EOSIO_ABI( teleos, (create) )
